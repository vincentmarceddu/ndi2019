const request = require('request');
const querystring = require('querystring');

const askLuisController = {};

let endpoint =
    "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/";
let luisAppId = "592b8c6a-e217-4e1a-a466-c9309d7a0945";
let endpointKey = "d606b38dc8d34a308867ef9ae054bec4";

askLuisController.ask = (req, res) => {
    let utterance = req.query.utterance;
    console.log(`Un utilisateur a demandé : '${utterance}'`);

    var queryParams = {
        "verbose": true,
        "q": utterance,
        "subscription-key": endpointKey
    }

    var luisRequest =
        endpoint + luisAppId +
        '?' + querystring.stringify(queryParams);

    request(luisRequest,
        function (err,
            response, body) {
            if (err)
                console.log(err);
            else {
                console.log("Got Luis answer : \n" + body);
                askLuisController.parse(body).then((parseAnswer) => {
                    res.send(parseAnswer);
                    res.end();
                });
            }
        });
};

const greetingsController = require('./intentControllers/greetingsController');
const presentItselfController = require('./intentControllers/presentItselfController');
const insultController = require('./intentControllers/insultController');
const informationRequestController = require('./intentControllers/informationRequestController');
const tellMeAJokeController = require('./intentControllers/tellMeAJokeController');
const howAreYouDoingController = require('./intentControllers/howAreYouDoingController');
const complimentController = require('./intentControllers/complimentController');
const ndiController = require('./intentControllers/ndiController');
const noneController = require('./intentControllers/noneController');

let intentControllerPair = {
    "Greetings":greetingsController,
    "Present_Itself":presentItselfController,
    "Insulte":insultController,
    "Information_Request":informationRequestController,
    "TellMeAJoke":tellMeAJokeController,
    "HowAreYouDoing":howAreYouDoingController,
    "Compliment":complimentController,
    "NDI":ndiController,
    "None":noneController
}

askLuisController.parse = (body) => {
    body = JSON.parse(body);

    let intentController = intentControllerPair[body.topScoringIntent.intent];

    return intentController.getAnswer(body);
}

module.exports = askLuisController;