$(document).ready(function(){
    $.ajax({
        url: "/api/score",
        cache: false,
        type: "GET",
        success: fillRanks,
        error: function (err) {
            console.log("API call error");
            console.error(err);
        }
    });
});

function fillRanks(scores){
    let text = "";
    const ordered = {};

    scores.sort((s1, s2) => {
        return (s1.score > s2.score) ? -1 : 0;
    }).forEach(score => {
        let userName = score["userName"];
        let userScore = score["score"];
        text += "<tr>\n";                
        text += "<td>" + userName + "</td>\n";
        text += "<td>" + userScore + "</td>\n";
        text += "</tr>\n";
    });
    $("#ranks tbody").append(text);
}