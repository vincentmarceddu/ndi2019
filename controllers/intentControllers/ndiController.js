const commonUtils = require('../../utils/commonUtils');
const LINQ = require("node-linq").LINQ;

const ndiController = {};

ndiController.jsonContent = require('./sentenceTemplates/ndi.json');

ndiController.getAnswer = (response) => {
    return new Promise((resolve, reject) => resolve(commonUtils.randomElement(ndiController.jsonContent)));
};

module.exports = ndiController;

