const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

// Luis
const askLuisController = require('./controllers/askLuisController');
app.get('/api/askLuis', askLuisController.ask);

// Ausy
const namingController = require('./controllers/namingControllers/namingController');
app.use('/api/naming', namingController);

// Scores
const scoreController = require('./controllers/errorGame/errorGameController');
app.use('/api/score', scoreController);

// Static files
app.use(express.static('public'));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.listen(3069, function() {
  console.log('NDI 2019 is UP and RUNNING on port 3069!');
});
