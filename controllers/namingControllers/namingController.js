const express = require('express');
const namingAndword = require('./themes.json');

let router = express.Router();

router.get('/themesAndWords', function(req, res){
    res.json(namingAndword);
});

module.exports = router;