const keyWords = ['var', 'let', 'const', 'class', 'function'];


let replacementThemesAndWords;
let currentReplacementWords;
const $replacementThemes = $('#replacementThemes');
$(document).ready(function() {
  $.ajax({
    url: '/api/naming/themesAndWords',
    cache: false,
    type: 'GET',
    success: function(themes) {
      replacementThemesAndWords = themes;
      currentReplacementWords = themes[Object.keys(themes)[0]];

      let themesNames = Object.keys(themes);
      if (Array.isArray(themesNames)) {
        themesNames.forEach(theme => {
          $replacementThemes.append(new Option(theme, theme));
        });
      }
    },
    error: function(err) {
      console.log('Themes and word error !');
      console.error(err);
    }
  });
});

$replacementThemes.on('change', function() {
  currentReplacementWords = replacementThemesAndWords[this.value];
  applyTransforms(); 
});

$('#transformButton').on('click', applyTransforms);

function applyTransforms(){
  let $transformText = $('#transformText');
  let $transformedCode = $('#transformedCode');

  //Split input text
  let splittedTransformText = $transformText.val().split(/\s+/);

  let variables = [];

  keyWords.forEach(keyword => {
    //Get all current keyword's indexes in splitted text
    let keyWordIndexes = splittedTransformText.indexOfAll(keyword);

    //Get all corresponding unique variables names
    keyWordIndexes.forEach(keywordIndex => {
      let keywordIndexIncrement = 0;

      //In case of multiple keywords
      while (
        keyWords.indexOf(
          splittedTransformText[keywordIndex + ++keywordIndexIncrement]
        ) != -1
      ) {}
      let variableName =
        splittedTransformText[keywordIndex + keywordIndexIncrement];
      if (variables.indexOf(variableName) === -1) variables.push(variableName);
    });
  });

  //Replace all variables
  let baseText = $transformText.val();
  let replacementWordsCopy = currentReplacementWords;
  variables.forEach(variable => {
    let replacingName = replacementWordsCopy.pop();
    baseText = baseText.replaceAll(variable, replacingName);
  });

  $transformedCode.val(baseText);
}

Array.prototype.indexOfAll = function(searchItem) {
  let array = this;
  var i = array.indexOf(searchItem),
    indexes = [];
  while (i !== -1) {
    indexes.push(i);
    i = array.indexOf(searchItem, ++i);
  }
  return indexes;
};

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.replace(new RegExp(`\\b${search}\\b(?!')`, 'g'), replacement);
};
