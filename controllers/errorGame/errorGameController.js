const express = require('express');
const fs = require('fs');

let scores = require('./scores.json');

let router = express.Router();

router.get('/', function(req, res){
    res.json(scores);
});

router.post('/', function(req, res){
    let userName =  req.body['userName'];
    let userScore =  req.body['score'];

    if(!!userName && !!userScore){
        if(!!scores[userName]){
            if(scores[userName] < userScore){
                scores.push({ userName: userName, score: userScore});
            } 
        } else {
            scores.push({ userName: userName, score: userScore});
        }
    
        res.status(200).send();
    } else {
        res.status(400).send();
    }
    
});

setInterval(saveScores, 10000);

function saveScores(){
    let scoresJson = JSON.stringify(scores);
    fs.writeFile(__dirname + "/scores.json", scoresJson, "utf8", function (err) {
        if (err) {
            console.log("An error occured while writing JSON Object to File.");
            console.log(err);
        }
    });
}

module.exports = router;