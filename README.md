![](https://i.ibb.co/920K6kH/logo.png)

# Table des matières

 1. Le Ballet des Dauphins Chanteurs
 2. Procédure d'installation en local
 3. Choix techniques


# 1. Le Ballet des Dauphins Chanteurs

Nous sommes 5 étudiants à l’école d’ingénieur du CNAM. Le Ballet des Dauphins Chanteurs, composé de cétacés rieurs, décident de relever le défi principal car c'est un sujet qui nous intéresse. Nous avons décidé de se focaliser sur la partie **dashboard** avec une page comprenant des outils de monitoring de l'environnement et la santé de la personne.


# 2. Procédure d'installation en local

Pour installer notre superbe site en local, c'est très simple ! 
Il vous suffit de cloner le repository Git en utilisant la commande suivante :
`git clone https://gitlab.com/vincentmarceddu/ndi2019`
Puis d'installer Node.js sur votre machine. https://nodejs.org/en/download/
Ensuite, vous pourrez lancer le serveur node js avec la commande `node app.js`, puis vous allez pouvoir accéder à la page grâce au lien "127.0.0.1:3000" dans votre navigateur favoris.


# 3. Choix techniques et comportement

En ce qui concerne les choix techniques, nous avons choisi **node.js**.
Nous étions à l'aise avec cette technologie et elle nous permettrait de fabriquer des éléments rapidement.

Pour la création des charts, nous avons utilisé **chart.js**. C'est un script open source comprenant tous les éléments pour notre dashboard. Il se découpe en 3 sections. Une section sur le GPS ainsi que les besoins vitaux de notre aventurier. Une partie avec un corps humain où l'on peut passer sur les différentes parties du corps et avoir des informations les concernant. Une dernière partie, sur l'environnement et les déplacements de la personne.