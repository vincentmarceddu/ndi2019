const commonUtils = require('../../utils/commonUtils');
const request = require('request');
const querystring = require('querystring');
const LINQ = require("node-linq").LINQ;

const informationRequestController = {};
informationRequestController.jsonContent = require('./sentenceTemplates/informationRequest.json');

informationRequestController.getAnswer = (response) => {
    let answer;

	let keyPhrase = new LINQ(response.entities).Where((entity) => entity.type === "builtin.keyPhrase").Select((entity) => entity.entity).items[0];
	
	if(keyPhrase.toUpperCase() === "AGIXIS"){
		answer = [
			{ "label": `Attends je regarde dans ma base...`, "mood": "eustache-espiegle" },
			{ "label": `Ah oui je vois ! C'est la meilleure entreprise du monde ;)`, "mood": "eustache-espiegle" },
			{ "label": `<a href="https://www.agixis.com">Voici le lien de leur site !</a>`, "mood": "eustache-espiegle" }
		];
	} else {
		let answerType = commonUtils.randomElement(["openAnswer", "closeAnswer"]);
		if (answerType === "openAnswer") {
            let keyWord = new LINQ(response.entities).Where((entity) => entity.type === "builtin.keyPhrase").Select((entity) => entity.entity).items[0];
            answer = (!!keyWord) ? 
            informationRequestController.getFromWikipedia(keyWord) :  
            {"label":"J'ai riiiiien compris, tu peux reformuler ?", "mood":"eustache-colere"};
		} else { 
			answer = commonUtils.randomElement(informationRequestController.jsonContent.closeAnswer);
		}
	}

    return new Promise((resolve, reject) => resolve(answer));
};

let endpoint =
    "https://fr.wikipedia.org/w/api.php";

informationRequestController.getFromWikipedia = (keyPhrase) => {

    var queryParams = {
        "action": "opensearch",
        "search": keyPhrase,
        "limit": 1,
        "namespace": 0,
        "format": "json"
    }

    var wikipediaRequest =
        endpoint + '?' + querystring.stringify(queryParams);

    return new Promise(function (resolve, reject) {
        request(wikipediaRequest,
            function (err, response, body) {
                if (!err && response.statusCode == 200) {
                    console.log(body);
                    resolve([
                        {
                            "label": "J'suis de bonne humeur, voici les informations de wikipédia : ",
                            "mood": "eustache-normal"
                        },
                        {
                            "label": JSON.parse(body)[2][0],
                            "mood": "eustache-normal"
                        }]);
                } else {
                    reject(err);
                }
            });
    });
}


module.exports = informationRequestController;

