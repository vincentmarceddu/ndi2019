class Burger {

    nextAllowedIngredients = [];
    ingredients = [];
    graphicComponent;

    constructor(ingredients) {
        this.id = Utils.getRandomId();
        this.ingredients = ingredients;
        this.computeAllowedIngredients();
        let ingredientsChain = ingredients.map(ingredient => ingredient.name).join("-");
        this.graphicComponent = $("<img />", {id: this.id, class: "draggable ingredient", src: `/img/${ingredientsChain}.png`, "data-ingredients": ingredientsChain}); 
    }

    getIngredients = () => {
        return this.ingredients;
    }

    getLastIngredient = () => {
        return this.ingredients[this.ingredients.length - 1];
    }

    addIngredient = (ingredient) => {

        if(this.nextAllowedIngredients.includes(ingredient.name)) {
            this.ingredients.push(ingredient);
            let ingredientsChain = this.ingredients.map(ingredient => ingredient.name).join("-");
            $(this.graphicComponent)
            .attr("src", `/img/${ingredientsChain}.png`)
            .attr("data-ingredients", ingredientsChain);

            this.computeAllowedIngredients();
        
            let mealIndex = gameManager.meals.findIndex(meal => meal.id == this.id);
            if (mealIndex >= 0) {
                gameManager.meals.splice(mealIndex, 1);
                Utils.removeInUi(ingredient.graphicComponent);
                gameManager.addBurger(this);
                const dataX = $(this.graphicComponent).attr("data-x");
                const dataY = $(this.graphicComponent).attr("data-y");
                this.graphicComponent
                    .css("left", `${dataX}px`)
                    .css("top", `${dataY}px`)
                    .attr("data-x", dataX)
                    .attr("data-y", dataY);
            }
            
        } else {
            let sound = soundManager.getSound("mergeError");
            sound.audio.play();
            console.error("Couldn't add the ingredient to the burger : it's not following a recipe");
        }
    }

    computeAllowedIngredients = () => {


        //The burger is finished : there is more than one ingredient and the last one is a bread
        if(this.ingredients.length >= 1 && (this.ingredients[this.ingredients.length - 1].name.includes("Bread") || this.ingredients[this.ingredients.length - 1].name.includes("bread"))) {
            let sound = soundManager.getSound("mealCompleted");
            sound.audio.play();
            this.nextAllowedIngredients = [];
            this.graphicComponent.addClass("eatable");
        } else {
            //Retrieve the last ingredient name when there is an ingredient in the burger
            let lastIngredientName = this.ingredients.length ? this.ingredients[this.ingredients.length - 1].name : undefined;
                    
            //If there is an ingredient, get the next ingredient in each recipe in the game
            if (lastIngredientName) {
                this.nextAllowedIngredients = [];
                let filteredRecipes = gameManager.recipes.filter(recipe => recipe.ingredients[0].name == this.ingredients[0].name);
                for (let recipe of filteredRecipes) {
                    let ingredientIndex = recipe.getIngredientsName().findIndex((elem) => elem == lastIngredientName);
                    // Verify that a recipe contains more ingredient
                    if (ingredientIndex >= 0 && ((recipe.getIngredientsName().length - 1) >= ingredientIndex + 1)) {
                        this.nextAllowedIngredients.push(recipe.getIngredientsName()[ingredientIndex + 1]);
                    } else {
                        console.warn("There is no more ingredient in this recipe");
                    }
                }
            }
        }
    }
}

class Utils {

    static getRandomId() {
        return Math.random().toString(36).substring(2, 12);
    }

    static displayInUi(component) {
        gameContainer.append(component);
    }

    static removeInUi(component) {
        $(component[0]).remove();
    }

    static removeDropzones() {
        $.each($(`.ingredient`), (index, elem) => {
            $(elem).removeClass("dropzone");
        });
    }

    static RandomizePosition(container) {
        const width = container.width();
        const height = container.height();
        return {x: Utils.getRandomInt((width / 100) * 7, (width / 100) * 93), y: Utils.getRandomInt((height / 100) * 20, (height / 100) * 80)};
    }

    static getRandomInt (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    
}

class Ingredient {

    graphicComponent;

    constructor (name, src) {
        this.id = Utils.getRandomId();
        this.name = name;
        this.src = src;
        const randomPosition = Utils.RandomizePosition($("body"));
        this.graphicComponent = $("<img />", {id: this.id, class: "draggable ingredient", src, "data-ingredients": name, "data-x": randomPosition.x, "data-y": randomPosition.y});

        this.graphicComponent
            .css("left", `${randomPosition.x}px`)
            .css("top", `${randomPosition.y}px`);
    }

}
class Recipe {

    ingredients = [];

    constructor (points, ingredients) {
        this.points = points;
        this.ingredients = ingredients;
    }

    getIngredientsLength = () => {
        return this.ingredients.length;
    }

    //Return an array containg the ingredients name. 
    // @Example ["Tomato", "Salad", "Steack"]
    getIngredientsName = () => {
        return this.ingredients.map( (ingredient) => {
            return ingredient.name;
        })
    }
}

class Sound {
    constructor (name, audio) {
        this.name = name;
        this.audio = audio;
    }
}

class SoundManager {

    sounds = [];

    constructor () {
        this.sounds.push(new Sound("cronch", new Audio("/sound/Cronch.mp3")));
        this.sounds.push(new Sound("cronchFavorite", new Audio("/sound/CronchFavorite.mp3")));
        this.sounds.push(new Sound("gaugeUp", new Audio("/sound/GaugeUp.mp3")));
        this.sounds.push(new Sound("mealCompleted", new Audio("/sound/MealCompleted.mp3")));
        this.sounds.push(new Sound("mergeSuccess", new Audio("/sound/MergeSuccess.mp3")));
        this.sounds.push(new Sound("mergeError", new Audio("/sound/MergeError.mp3")));
        this.sounds.push(new Sound("backgroundMusic",  new Audio("/sound/BackgroundMusic.mp3")));
    }

    getSound = (name) => {
        return this.sounds.find(sound => sound.name == name)
    }
}

class GameManager {
    
    meals = [];
    ingredientPopping;
    limitIncreasing;
    speedIncreasing;

    constructor () {
        this.popTimer = 1500;
        this.limitTimer = 5000;
        this.mealsLimit = 10;
        this.recipes = [];
        this.score = 0;
    }

    addRecipe = (recipe) => {
        this.recipes.push(recipe);
    }

    addMeal = (meals) => {
        this.meals.push(meals);
        Utils.displayInUi(meals.graphicComponent);
    }

    addBurger = (burger) => {
        this.meals.push({...burger, type: "burger"});
        Utils.displayInUi(burger.graphicComponent);
        let sound = soundManager.getSound("mergeSuccess");
        sound.audio.play();
    }

    addIngredient = (ingredient) => {
        this.meals.push({...ingredient, type: "ingredient"});
        Utils.displayInUi(ingredient.graphicComponent);
    }

    removeIngredient = (ingredient) => {
        
        let mealIndex = this.meals.findIndex(meal => meal.id == ingredient.id);
        if (mealIndex >= 0) {
            this.meals.splice(mealIndex, 1);
            Utils.removeInUi(ingredient.graphicComponent);
        }
    }

    ingredientsOccurences = ["bread", "bread", "bread", "bread", "wholemealBread", "wholemealBread", "wholemealBread", "wholemealBread", "wholemealBread", "wholemealBread", "tomato", "tomato", "tomato", "steak", "steak", "steak", "lettuce", "lettuce", "cheese", "cheese", "cheese", "pickle"];

    generateIngredient = () => {
        const index = Utils.getRandomInt(0, this.ingredientsOccurences.length - 1);
        let imageName = this.ingredientsOccurences[index];
        imageName[0]
        return new Ingredient(this.ingredientsOccurences[index], `/img/${this.ingredientsOccurences[index]}.png`);
    }

    startGame = () => {

        this.updateGauge(this.meals.length / this.mealsLimit);

        this.ingredientPopping = setInterval(() => {
            if(this.meals.length < this.mealsLimit) {
                this.addIngredient(this.generateIngredient());
                this.updateGauge(this.meals.length / this.mealsLimit);
            } else {
                this.loseGame();
            }
        }, this.popTimer);
        
        this.limitIncreasing = setInterval(() => {
            //increase max limit
            this.mealsLimit < 50 ? this.mealsLimit++ : this.mealsLimit;
            this.updateGauge(this.meals.length / this.mealsLimit);
        }, this.limitTimer);
        
        this.speedIncreasing = setInterval(() => {
            //increase max limit
            this.popTimer > 500 ? this.popTimer =- 50 : this.popTimer;
        }, this.speedTimer);
    }

    loseGame = () => {
        document.getElementById("loser").style.display = "inline-block";
    }

    stopGame = () => {
        clearInterval(this.ingredientPopping);
        clearInterval(this.limitIncreasing);
        clearInterval(this.speedIncreasing);
    }

    getMeal = (id) => {
        return this.meals.find(meal => meal.id == id);
    }

    updateGauge = (percent) => {

        let liquid = document.getElementsByClassName('gauge-liquid')[0];
    
        let size = 80 * percent ;
    
        liquid.style.height = size + "vh";
    
    }

    setScore = (name, score) => {
        $.post({
            url: "/api/score",
            data: {
                "userName": name,
                "score": score
            },
            cache: false,
            success: function(res){
            }, 
            error: function(err){
                console.error("error", err)
            }
        });
    }

}

var gameContainer = $("#game-container");
var playPauseBtn = $("#playPauseButton");


let bread = new Ingredient("bread", "/img/bread.png");
let steak = new Ingredient("steak", "/img/steak.png");
let cheese = new Ingredient("cheese", "/img/cheese.png");
let lettuce = new Ingredient("lettuce", "/img/lettuce.png");
let wholemealBread = new Ingredient("wholemealBread", "/img/wholemealBread.png");
let pickle = new Ingredient("pickle", "/img/pickle.png");
let tomato = new Ingredient("tomato", "/img/tomato.png");

let recipe = new Recipe(50, [bread, steak, cheese, bread]);
let recipe2 = new Recipe(50, [wholemealBread, cheese, tomato, wholemealBread]);
let recipe3 = new Recipe(100, [wholemealBread, tomato, lettuce, wholemealBread]);
let recipe4 = new Recipe(200, [bread, steak, cheese, lettuce, bread]);
let recipe5 = new Recipe(500, [wholemealBread, steak, tomato, pickle, wholemealBread]);

var gameManager = new GameManager();
var soundManager = new SoundManager();

gameManager.addRecipe(recipe);
gameManager.addRecipe(recipe2);
gameManager.addRecipe(recipe3);
gameManager.addRecipe(recipe4);
gameManager.addRecipe(recipe5);
gameManager.addIngredient(bread);
gameManager.addIngredient(steak);
gameManager.addIngredient(tomato);

playPauseBtn.on("mousedown", (event) => {
    let sound = soundManager.getSound("backgroundMusic");
    sound.audio.loop = true;
    sound.audio.volume = 0.3;
    // sound paused
    if(sound.audio.paused) {
        sound.audio.play();
    }
    // sound playing
    else {
        sound.audio.pause();
    }
})

// let burger = new Burger([bread3, tomato3]);
// gameManager.addBurger(burger);

//Whenever a ingredient is selected, compute with which ingredient he could be merged
$("#game-container").on("mousedown", ".ingredient", (event) => {
    // Iterate through gameManager.meals, and retrieve the object regarding the event
        // Take is nextAllowedIngredients if its exist, then compute the 'accept' classes for interacting with dropzone
        // Else (no nextAllowedIngredients, so it's a ingredient), allo bread or every burger that got the good ingredients from the start of a recipe 

    let meal = gameManager.getMeal(event.target.id);
    if (meal) {
        //The meal is a ingredient
        if (meal.type == "ingredient") {
            if (meal.name.includes("bread") || meal.name.includes("Bread")) {
                //Get every ingredient that's just after the bread in a recipe, since it's the base of the burger
                let filteredRecipes = gameManager.recipes.filter(recipe => recipe.ingredients[0].name == meal.name);
                let ingredients = filteredRecipes.map(recipe => recipe.ingredients[1].name); 
                for(let ingredient of ingredients) {
                    $.each($(`[data-ingredients="${ingredient}"]`), (index, elem) => {
                        $(elem).addClass("dropzone");
                    });
                }
                
                // Get burger from gameManager, and check their next ingredients
                // If the next ingredient corresponds to the current ingredient, add its id
                for (let m of gameManager.meals) {
                    if (m.type == "burger" && m.nextAllowedIngredients.includes(meal.name)) {
                        $.each($(`#${m.id}`), (index, elem) => {
                            $(elem).addClass("dropzone");
                        });
                    }   
                }
            } else {
                //If the ingredient is the second one in a recipe, allow bread. 
                //Get burgers and check if the current ingredient is the next step of the recipe

                let dropOnBreadAllowed = gameManager.recipes.some(recipe => recipe.ingredients[1].name == meal.name);
                
                if (dropOnBreadAllowed) {
                    $.each($(`[data-ingredients="${"Bread"}"],[data-ingredients="${"WholemealBread"}"]`), (index, elem) => {
                        $(elem).addClass("dropzone");
                    });
                }

                // Get burger from gameManager, and check their next ingredients
                // If the next ingredient corresponds to the current ingredient, add its id
                for (let m of gameManager.meals) {
                    if (m.type == "burger" && m.nextAllowedIngredients.includes(meal.name)) {
                        $.each($(`#${m.id}`), (index, elem) => {
                            $(elem).addClass("dropzone");
                        });
                    }   
                }
            }
        }
        else { // The meal is a burger
            // @Todo if we select a burger, we can drop it on the following ingredient of the recip
            // If there is no more ingredient, the recipe is complete => feed the monster
            const dataIngredients = meal.nextAllowedIngredients.map(ingredient => `[data-ingredients="${ingredient}"]`);
            $.each($(dataIngredients.join(",")), (index, elem) => {
                $(elem).addClass("dropzone");
            });
        }
    }
})

$(".ingredient").on("mouseup", (event) => {
    Utils.removeDropzones();
})

interact(".draggable")
    .draggable({
        inertia: false,
        onmove: dragMoveListener,
        onend: function(evt) {
        }
    });

interact('.dropzone')
    .dropzone({
        overlap: 0.4,
        accept: ".ingredient",
        ondropactivate: function (event) {
            event.target.classList.add("drop-active");
        },
        ondrop: function (event) {

            let firstIngredient = gameManager.getMeal(event.relatedTarget.id);
            let secondIngredient = gameManager.getMeal(event.target.id);

            // ingredient + ingredient => create a burger 
            let burger;
            if (firstIngredient.type == "ingredient" && secondIngredient.type == "ingredient") {
                if (firstIngredient.name.includes("Bread") || firstIngredient.name.includes("bread")) {
                    burger = new Burger([firstIngredient, secondIngredient]);
                } else {
                    burger = new Burger([secondIngredient, firstIngredient]);
                }
                gameManager.removeIngredient(firstIngredient);
                gameManager.removeIngredient(secondIngredient);
                
                const dataX = $(secondIngredient.graphicComponent).attr("data-x");
                const dataY = $(secondIngredient.graphicComponent).attr("data-y");
                
                $(burger.graphicComponent)
                    .css("left", `${dataX}px`)
                    .css("top", `${dataY}px`)
                    .attr("data-x", dataX)
                    .attr("data-y", dataY);
                gameManager.addBurger(burger);
                
            } else if (firstIngredient.type == "burger" && secondIngredient == "burger") {
                console.error("/!\ Impossible case : Additionning two burgers");
            } else if (firstIngredient.type == "burger") {
                // Add the ingredient to the burger, delete the ingredient
                gameManager.removeIngredient(secondIngredient);
                firstIngredient.addIngredient(secondIngredient);
                
            } else { //Means that second ingredient is a burger
                gameManager.removeIngredient(firstIngredient);
                secondIngredient.addIngredient(firstIngredient);
            }

            Utils.removeDropzones();
            gameManager.updateGauge(gameManager.meals.length / gameManager.mealsLimit);
        },
        ondropdeactivate: function (event) {
            event.target.classList.remove("drop-active");
        },
    });

interact('.eat-dropzone')
    .dropzone({
        overlap: 0.4,
        accept: ".eatable",
        ondropactivate: function (event) {
            event.target.src = "/img/monstre-ouvert.png";
        },
        ondropdeactivate: function (event) {
            event.target.src = "/img/monstre-ferme.png";
            let meal = gameManager.getMeal(event.relatedTarget.id);
            let score = 0; 
            if ( meal.ingredients.length == 4 ) {
                score = 100;
            } else if ( meal.ingredients.length == 5) {
                score = 200;
            } else {
                score = 75;
            }
            if (meal.ingredients.some(ingredient => ingredient.name == "Pickle")) {
                score += 50;
            }
            gameManager.score += score;
            $("#score").text(gameManager.score);
        },
        ondrop: function (event) {
            document.getElementById(event.relatedTarget.id).remove();
            soundManager.getSound("cronch").audio.play();
        }
    });

function dragMoveListener(event) {

    let target = event.target

    // Keep the dragged position in the data-x/data-y attributes
    const x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
    const y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // Translate the element
    target.style.left = `${x}px`;
    target.style.top = `${y}px`;

    // Update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
}

$("#buttonScore").on("click", (event) => {
    gameManager.setScore($("#pseudo").text(), gameManager.score);
})

$("#error").on("click", (event) =>{

    $("#error").css("display", "none");
    gameManager.startGame();
    gameManager.addIngredient(bread);
    gameManager.addIngredient(steak);
    gameManager.addIngredient(tomato);
})
