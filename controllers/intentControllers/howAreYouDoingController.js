const commonUtils = require('../../utils/commonUtils');

const howAreYouDoingController = {};
howAreYouDoingController.jsonContent = require('./sentenceTemplates/howAreYouDoing.json');

howAreYouDoingController.getAnswer = (response) => {
    return new Promise((resolve, reject) => resolve(commonUtils.randomElement(howAreYouDoingController.jsonContent)));
};

module.exports = howAreYouDoingController;

